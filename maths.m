syms Vo Vi V Omega_z R L B  ii io iboth 'r' 'omega'
Omega_z=Vi*((Vo*(1-io)/Vi-(1-ii))/B)
-(Vi*((Vo*(io - 1))/Vi - ii + 1))/B
R=B/2*(Vo*(1-io)+Vi*(1-ii))/(Vo*(1-io)-Vi*(1-ii))
% R =
% -(B*(Vi*(ii - 1) + Vo*(io - 1)))/(2*(Vi*(ii - 1) - Vo*(io - 1)))
assume(ii < 1)
assume(io < 1)
assumeAlso(ii >= 0)
assumeAlso(io >= 0)
assume(omega ~= 0)
[vo_sol, vi_sol, ~, cond] = solve(R==r, Omega_z==omega, Vi, Vo, 'ReturnConditions', true)
% vo_sol
% (B*omega - 2*omega*r)/(2*ii - 2)
% vi_sol
% -((B*omega)/2 + omega*r)/(io - 1)
% cond 
% TRUE
simplify(expand(vi_sol/vo_sol))
% -((B + 2*r)*(ii - 1))/((B - 2*r)*(io - 1))
simplify(expand(subs(vi_sol/vo_sol, [ii, io], [iboth iboth])))
