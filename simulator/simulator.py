import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
Writer = animation.writers['ffmpeg']
writer = Writer(fps=20, metadata=dict(artist='Me'), bitrate=1800)
xy = [(1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4),
      (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), (1, 2), (2, 2), (3, 2), (4, 3), (4, 4), ]
from matplotlib.patches import Rectangle, Polygon
from math import pi
from math import cos, sin, radians

nx = 2400
ny = 1200
xy = [(p[0] * nx / 6, p[1] * ny / 6) for p in xy]
fig = plt.figure(figsize=(nx / 200, ny / 200))
plt.xlim(0, nx)
plt.ylim(0, ny)
ax = plt.gca()
time_text = ax.text(10, 10, '', horizontalalignment='left',
                    verticalalignment='top')

# rect = Rectangle(xy[1], nx / 10, ny / 10, facecolor='c')
poles = [Rectangle((p[0] - 20, p[1] - 20), 40, 40, facecolor='r') for p in [(1200 - 500 / 2 - 20, 20),
                                                                            (1200 + 500 / 2 + 20, 20), (1200 - 500 / 2 - 20, 560), (1200 + 500 / 2 + 20, 560)]]
boxes = [
    Rectangle((0, 0), 45, 1200, color='g'),
    Rectangle((250 + 45, 0), 45, 1200, color='g'),
    Rectangle((2400 - 45, 0), 45, 1200, color='g'),
    Rectangle((2400 - 45 - 250, 0), 45, 1200, color='g'),
    Rectangle((45, 0), 250, 45, color='g'),
    Rectangle((45, 1200 - 45), 250, 45, color='g'),
    Rectangle((2400 - 250 - 45, 0), 250, 45, color='g'),
    Rectangle((2400 - 250 - 45, 1200 - 45), 250, 45, color='g'),
]


class Robot:
    chassis_width = 350
    chassis_length = 300
    arm_width = 475
    arm_length = 150
    arm_offset_l = 50
    arm_offset_w = -20
    wheelbase_width = 325
    max_ustep = 16
    steps = 200
    wheel_dia = 80
    delta = pi * wheel_dia / (steps * max_ustep)

    def calculate_next(self):
        if v_l == v_r:
            pass
        theta = radians(self.angle)
        c = cos(theta)
        s = sin(theta)
        self.x = self.x + c * self.vl - s * self.vl
        # self.y =

    def drive(self, distance):
        theta = radians(self.angle)
        c = cos(theta)
        s = sin(theta)
        self.x = self.x + c * distance
        self.y = self.y + s * distance
        self.update()

    def unfold(self):
        self.arm_folded = False

    def pivot(self, angle):
        self.angle += angle
        self.update()

    def update(self):
        print("x: {}\ty: {}\t angle:{}".format(self.x, self.y, self.angle))
        self._outline.set_xy(self.rotated_outline())

    def draw_init(self, ax):
        ax.add_patch(self._outline)

    def base_outline(self):
        return [
            (-self.chassis_length / 2, -self.chassis_width / 2),
            (self.chassis_length / 2, -self.chassis_width / 2),
            (self.chassis_length / 2 + self.arm_offset_l, -
             self.arm_offset_w - self.arm_width / 2),
            (self.chassis_length / 2 + self.arm_offset_l +
             self.arm_length, -self.arm_offset_w - self.arm_width / 2),
            (self.chassis_length / 2 + self.arm_offset_l + self.arm_length, -
             self.arm_offset_w + self.arm_width / 2 + (self.arm_width if not self.arm_folded else 0)),
            (self.chassis_length / 2 + self.arm_offset_l, -
             self.arm_offset_w + self.arm_width / 2 + (self.arm_width if not self.arm_folded else 0)),
            (self.chassis_length / 2, self.chassis_width / 2),
            (-self.chassis_length / 2, self.chassis_width / 2),
        ]

    def rotated_outline(self):
        theta = radians(self.angle)
        c = cos(theta)
        s = sin(theta)
        return [(self.x + (c * p[0] - s * p[1]), self.y + (s * p[0] + c * p[1])) for p in self.base_outline()]

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.angle = 0.0
        self.v_l = 0
        self.v_r = 0
        self.arm_folded = True
        self._outline = Polygon(self.rotated_outline(), closed=True)


r = Robot(1200 - 100, 250 + 20)


def drive(distance):
    distance *= -1
    i = 0
    sign = -1 if distance < 0 else 1
    while (i < abs(distance)):
        r.drive(sign)
        i += 1
        yield(i)


def pivot(angle):
    angle *= -.65  # adjust for slippage and sign convention
    i = 0
    sign = -1 if angle < 0 else 1
    while (i < abs(angle)):
        r.pivot(sign)
        i += 1
        yield(i)


def unfold():
    r.unfold()
    yield True


def init():
    r.draw_init(ax)
    for p in poles:
        ax.add_patch(p)
    for b in boxes:
        ax.add_patch(b)
    time_text.set_text(0)
    return poles + [r, time_text]


def movement(r):
    from itertools import chain
    moves = chain(
        drive(-300),
        drive(-200),
        pivot(-25),
        drive(-200),
        pivot(25),
        drive(-250),
        unfold(),
        drive(140),
        pivot(90),
        drive(500),
        pivot(-85),
        drive(1000),
        pivot(-245),
        drive(-200),
        pivot(40),
        drive(-170),
    )
    for m in moves:
        yield(m)


rm = movement(r)


def animate(i):
    print("animate({})".format(i))
    ax = plt.gca()
    next(rm)
    time_text.set_text('time = %.1d' % i)
    return [time_text, r]
    # plt.line


ani = matplotlib.animation.FuncAnimation(
    fig, animate, init_func=init, frames=60000, interval=(1000 / 25)/10)

plt.show()
