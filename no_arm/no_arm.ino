#include <Arduino.h>
#include <Tic.h>
#include <DRV8835MotorShield.h>
#include <math.h>
// Following the Tic library directions:
// On boards with a hardware serial port available for use, use
// that port to communicate with the Tic. For other boards,
// create a SoftwareSerial object using pin 10 to receive (RX)
// and pin 11 to transmit (TX).

#ifdef SERIAL_PORT_HARDWARE_OPEN
#define ticSerial SERIAL_PORT_HARDWARE_OPEN
#else
#include <SoftwareSerial.h>
SoftwareSerial ticSerial(10, 11);
#endif
//#define DEBUG 1
#undef DEBUG
#ifdef DEBUG
#define test_delay(t) delay(t)
#define dbp(str) \
  do{ \
    Serial.print(__PRETTY_FUNCTION__); \
    Serial.print("\t"); \
    Serial.print(__LINE__); \
    Serial.print(":\t");\
    Serial.print(str);\
  }while(false)
#define dbpl(str) \
  do{ \
    Serial.print(__PRETTY_FUNCTION__); \
    Serial.print("\t"); \
    Serial.print(__LINE__); \
    Serial.print(":\t");\
    Serial.println(str);\
  }while(false)
#else
#define test_delay(t) do{}while(false)
#define dbp(str) do{}while(false)
#define dbpl(str) do{}while(false)
#endif

#define masterSwitch  0
uint8_t phase = 0;

const TicStepMode mode = TicStepMode::Microstep8;
const uint32_t maxSpeed = 134290000;
const uint32_t maxAccel = 320000;
const uint32_t maxDecel = 240000;
const bool reverse = false;
const double DIAMETER = 80.0;
// const double PI = 3.141592653589793;
const int FULL_STEPS = 200;
const double WHEEL_SPACING = 320;
#define DEG_TO_RAD_F(deg) ((typeof(deg)) ((deg) * PI/180.0))
#define DIR (reverse ?  -1 : 1)
const double SLIP = 0.0;
const double STEPS_PER_REV = (double) (FULL_STEPS * (1 << (int) mode));
const double MM_PER_STEP = DIAMETER * PI / STEPS_PER_REV * (1.0 - SLIP);
const double STEP_PER_MM = 1 / MM_PER_STEP;
const uint32_t STEP_PER_NM = ((uint32_t) (1000.0 * STEP_PER_MM));
// #define MM_TO_STEPS(mm) ((int32_t)((double)mm)/MM_PER_STEP)
inline int32_t MM_TO_STEPS(double mm)
{
  double dsteps = DIR * mm / MM_PER_STEP;
  return (int32_t) dsteps;
}


DRV8835MotorShield motors;
int16_t m1speed=0;
int16_t m2speed=0;
#define DC_STEPS_PER_REV ((64/2)/2)
#define DC_MM_PER_REV 8
//#define DC_STEPS_PER_MM ( DC_STEPS_PER_REV / DC_MM_PER_REV )
#define DC_STEPS_PER_MM 1

#define CURVE_ENCODER 2
int16_t curveTarget;
bool curveDir;
int16_t curveCount;
bool curveDone = true;
#define LIFT_ENCODER 3
int16_t liftTarget;
bool liftDir;
int16_t liftCount;
bool liftDone = true;

#define MS1_PIN 20
void ms1_isr(){
  dbpl("ms1 triggered");
}
#define MS2_PIN 21
void ms2_isr(){
  dbpl("ms2 triggered");
}
/*
  Each Tic500 has its own address, as listed below, and has a secondary group address.
  Requests for data, that is get...() functions, should always be sent to an individual Tic, becasue the response will be nonsense if the messages are not identical.
  Commands, that is set...() functions, should usually be sent to a pair
*/
TicSerial left_front(ticSerial, 11);
TicSerial left_back(ticSerial, 12);
TicSerial left(ticSerial, 10);
TicSerial right_front(ticSerial, 21);
TicSerial right_back(ticSerial, 22);
TicSerial right(ticSerial, 20);
//
// TicSerial leftGroup[] = {left, left_front, left_back};
// TicSerial rightGroup[] = {right, left_front, left_back};
//
/*
  Track poisition on the arduino to save polling the Tics.
*/


int32_t leftPos = 0;
int32_t rightPos = 0;
void setup()
{
  phase = 0;
#ifdef DEBUG
  Serial.begin(115200);
#endif
  pinMode(masterSwitch, INPUT_PULLUP);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  motors.flipM1(true);
  attachInterrupt(digitalPinToInterrupt(LIFT_ENCODER), lift_upencoder, RISING);
  attachInterrupt(digitalPinToInterrupt(CURVE_ENCODER), curve_upencoder, RISING);
  
//  pinMode(MS1_PIN, INPUT_PULLUP);
//  attachInterrupt(digitalPinToInterrupt(MS1_PIN), ms1_isr, LOW);
//  pinMode(MS2_PIN, INPUT_PULLUP);
//  attachInterrupt(digitalPinToInterrupt(MS2_PIN), ms2_isr, LOW);
  digitalWrite(13, LOW);
  // Set baud rate for serial comms
  ticSerial.begin(115385);
  // Pause to allow comms to be established: required by Pololu protocol
  delay(20);
  // Pause to allow the USB cable to be removed etc.
//  dbpl("10 s delay");
//  test_delay(10000);
  // Unlock the motors to allow commands to be acted upon
  left.exitSafeStart();
  right.exitSafeStart();
  // Set current limits etc. and energize motors
  keyDefaults(left);
  keyDefaults(right);
  left.energize();
  right.energize();
  // Clear any errors which may have occurred: in the competiton there's no way we can fix a problem anyway ,so just hope the problem will go away if we ignore it.
  left.clearDriverError();
  right.clearDriverError();
  left.exitSafeStart();
  right.exitSafeStart();
  // Set speed etc. limits
  restoreDefaults(left);
  restoreDefaults(right);
  // Clear errors again
  left.exitSafeStart();
  right.exitSafeStart();
  digitalWrite(13, LOW);
  dbpl("started");
}

/*
  Convenience method to stop the motors and reset the position to 0
*/
void hsp()
{
  left.haltAndSetPosition(0);
  right.haltAndSetPosition(0);
  leftPos = 0;
  rightPos = 0;
}

/*
  Sets the essential settings which should not be changed for a motor
*/
void keyDefaults(TicSerial & which)
{
  which.setProduct(TicProduct::T500);
  which.setCurrentLimit(1200);
  which.haltAndSetPosition(0);
}

/*
  set those defaults which can reasonably be overridden during operation
*/
void restoreDefaults(TicSerial & which)
{
  which.setStepMode(mode);
  which.setMaxSpeed(maxSpeed);
  which.setMaxAccel(maxAccel);
  which.setMaxDecel(maxDecel);
}

/*
  Drive the specified distance in millimeters.
  Note: this overshoots slightly, by around 1%, as the wheels are marginally oversized.
*/
bool drive(double millimetres)
{
  dbpl(millimetres);
  // Calculate the target positions
  int32_t left_target = leftPos + MM_TO_STEPS(millimetres);
  int32_t right_target = rightPos + MM_TO_STEPS(millimetres);
  left.setTargetPosition(left_target);
  right.setTargetPosition(right_target);
  dbpl("set");
//  waitForPosition(right_front, right_target);
    dbpl("right pos");
  waitForPosition(left_front, left_target);
    dbpl("leftpos");
  leftPos = left_target;
  rightPos = right_target;
  return true;
}

// inline uint32_t reduceByRatio(uint32_t base, double ratio){
// return (uint32_t)(((double) base)/ratio);
// }
#define reduceByRatio(base, ratio) ((typeof(base)) (((typeof(ratio)) base) /ratio))
// Positive radius, positive angle is forwards
/*

*/
bool curve(double radius, double angle, bool turn_right)
{
  double theta = DEG_TO_RAD_F(angle);
  // assert(radius >= 0);
  // The ratio of inner and outer distances is given by -(B + 2*r)/(B - 2*r) if their slip is constant
  if (WHEEL_SPACING == 2 * radius)
  {
    radius += 1;
  }
  double ratio = - (WHEEL_SPACING + 2 * radius) / (WHEEL_SPACING - 2 * radius);
  uint32_t inner_speed = reduceByRatio(maxSpeed, ratio);
  uint32_t inner_accel = reduceByRatio(maxAccel, ratio);
  uint32_t inner_decel = reduceByRatio(maxDecel, ratio);
  int32_t outer_distance = radius * DEG_TO_RAD_F(angle) + WHEEL_SPACING / 2;
  int32_t inner_distance = radius * DEG_TO_RAD_F(angle) - WHEEL_SPACING / 2;
  TicSerial inner = turn_right ? right : left;
  inner.setMaxAccel(inner_accel);
  inner.setMaxSpeed(inner_speed);
  inner.setMaxDecel(inner_decel);
  restoreDefaults(inner);
  return false;
}

/*
  Pivots about the centre of the chassis.
  Positive angles are clockwise when viewed from above.
  Becuase no allowance has yet been made for wheelslip, the rotation is approximately 3/4 of the specificed angle in degrees.
*/
bool pivot(double angle)
{
  dbpl(angle);
  // The angular velocity = 2v/(WHEEL_SPACING)
  // therefore,  angle = 2v/(WHEEL_SPACING)*t = 2d/(WHEEL_SPACING)
  angle = DEG_TO_RAD_F(angle);
  double millimetres = WHEEL_SPACING * angle / 2.0;
  // Note that unlike in drive() the distance is added to the right but subtracted from the left
  int32_t left_target = leftPos - MM_TO_STEPS(millimetres);
  int32_t right_target = rightPos + MM_TO_STEPS(millimetres);
  // //  Reduce speed and acceleration for pivoting becuase of the higher torque required.
  // uint32_t pivot_accel = maxAccel/16;
  // uint32_t pivot_speed = maxSpeed/16;
  // left.setMaxAccel(pivot_accel);
  // right.setMaxAccel(pivot_accel);
  // left.setMaxSpeed(pivot_speed);
  // right.setMaxSpeed(pivot_speed);
  // This is the same as in drive()
  left.setTargetPosition(left_target);
  right.setTargetPosition(right_target);
  waitForPosition(right_front, right_target);
  waitForPosition(left_front, left_target);
  // Update the cached position
  leftPos = left_target;
  rightPos = right_target;
  // reset the speed and acceleration limits to the normal values
  restoreDefaults(left);
  restoreDefaults(right);
  return true;
}

// From the Tic Library examples:
// Polls the Tic, waiting for it to reach the specified target
// position.  Note that if the Tic detects an error, the Tic will
// probably go into safe-start mode and never reach its target
// position, so this function will loop infinitely.  If that
// happens, you will need to reset your Arduino.
void waitForPosition(TicBase & which, int32_t targetPosition)
{
  do
  {
    which.resetCommandTimeout();
  }
  while (which.getCurrentPosition() != targetPosition);
}

/*
  Halt and disable operation: this effectively prevents the loop continuing.
*/
void hcf()
{
  right.deenergize();
  left.deenergize();
}

#define curve_distance 3000
void curve_upencoder() {
//  dbpl("curve encoder rising edge detected");
  ++curveCount;
  dbpl(curveCount);
  if (curveCount >= curveTarget) {
    motors.setM2Speed(0);
   dbpl("reached curve target");
    m2speed = 0;
    curveDone = true;
  }
}

void curveOpen(int16_t steps, bool up) {
  dbpl(steps);
  curveCount = 0;
  curveDone = false;
  curveTarget = steps;
  curveDir = up;
  motors.setM2Speed((up ? -1 : 1) * 400);
  m2speed = ((up ? -1 : 1) * 400);
}

void lift_upencoder() {
//  dbpl("lift encoder");
  ++liftCount;
  dbpl(liftCount);
  if (liftCount >= liftTarget) {
    dbpl("reached lift target");
    motors.setM1Speed(0);
    m1speed = 0;
    liftDone = true;
  }
}

void liftMotor(int16_t steps, bool up) {
  dbpl(steps);
  liftCount = 0;
  liftDone = false;
  liftTarget = steps;
  liftDir = up;
  motors.setM1Speed((up ? -1 : 1) * 400);
  m1speed = ((up ? -1 : 1) * 400);
}

void loop()
{
  dbpl(phase);
//  motors.setM1Speed(m1speed);
//  motors.setM2Speed(m2speed);
  if (phase == 0) {
    //Loop until activation switch is fired
    if (digitalRead(masterSwitch)) {
      phase = 1;
      dbpl("activation switch on");
    } else {
      test_delay(5000);
    }
  }
  if (phase == 1) {
    dbpl("advacning out of garage");
    drive(-200);
    dbpl("nose out, activating curve");
//    curveOpen(curve_distance, true);
//  motors.setM2Speed(400);
//    delay(2000);
//    motors.setM2Speed(0);
    drive(-200);
    dbpl("fully out");
//     motors.setM1Speed(-400);
//      delay(2000);
//      motors.setM1Speed(0);
    pivot(-25);
    dbpl("B");
    drive(-200);
    dbpl("C");
    pivot(25);
    dbpl("D");
    drive(-250);
    dbpl("E");
    dbpl("driven to ocean compound, lowering arm");
//    liftMotor(30 * DC_STEPS_PER_MM, false);
//      motors.setM1Speed(400);
//      delay(2000);
//      motors.setM1Speed(0);
//      motors.setM2Speed(0);
      
      liftDone = true;
    phase = 2;
  }
  if (phase == 2) {
    if (!curveDone || !liftDone) {
      if (!curveDone) {
        dbpl("curve not done, waiting");
      }
      if (!liftDone) {
        dbpl("lift not done, waiting");
        dbpl(liftCount);
        dbpl(liftTarget);
      }
      test_delay(2000);
    } else {
      dbpl("lifing arm back up");
//      liftMotor(90 * DC_STEPS_PER_MM, true);
//      motors.setM1Speed(-400);
//      delay(12000);
//      motors.setM1Speed(0);
      curveDone = true;
      phase = 3;
    }
  }
  if (phase == 3) {
    if (liftDone) {
      dbpl("arm has cleared ocean compund wall, moving to deposit");
      phase = 4;
    } else {
      dbpl("waiting for the arm to clear the ocean compund wall");
    }
  }
  if (phase == 4) {
    dbpl("start lifting the arm to clear the inland compound wall");
//    liftMotor(90 * DC_STEPS_PER_MM, true);
//      motors.setM1Speed(-400);
//      delay(2000);
//      motors.setM1Speed(0);
      curveDone = true;
    dbpl("reverse to give space to turn");
    drive(140);
    dbpl("G");
    pivot(90);
    dbpl("H");
    drive(500);
    dbpl("I");
    pivot(-85);
    dbpl("J");
    drive(1000);
    dbpl("we can't go any futher until the arm is fully up");
    phase = 5;
  }
  if (phase == 5) {
    if (!liftDone) {
      dbpl("waiting to clear the inland compound wall");
      test_delay(2000);
    } else {
      dbpl("advance to the inland compound");
      //  dbpl("K");
      //    digitalWrite(13, HIGH);
      pivot(-245);
      dbpl("L");
      //    digitalWrite(13, LOW);
      drive(-200);
      dbpl("M");
      //    digitalWrite(13, HIGH);
      pivot(40);
      dbpl("N");
      drive(-170);
      dbpl("O");
      //    digitalWrite(13, HIGH);
//      drive(2000);
      dbpl("lower arm into the inland compund");
//      liftMotor(60 * DC_STEPS_PER_MM, false);
//      motors.setM1Speed(400);
//      delay(10000);
//      motors.setM1Speed(0);
      liftDone = true;
      phase = 6;
    }
  }
  if (phase == 6) {
    if (!liftDone) {
      dbpl("waiting for the deposit");
      test_delay(1000);
    } else {
      dbpl("done!!!!!!");
      hcf();
      phase = 7;
    }
  }
  if (phase == 7) {
    if (digitalRead(masterSwitch)) {
      dbpl("I'm done, wiating to be reset");
      delay(5000);
    } else {
      liftMotor((-30 + 90 + 90 - 60) * DC_STEPS_PER_MM, false);
//      motors.setM1Speed(400);
//      delay(1000)
//      motors.setM1Speed(0);
      curveOpen(curve_distance, false);
      phase = 8;
    }
  }
//  if (phase == 8) {
//    if (!curveDone || !liftDone) {
//      if (!curveDone) {
//        dbpl("curve not done, waiting");
//      }
//      if (!liftDone) {
//        dbpl("lift not done, waiting");
//      }
//      test_delay(2000);
//    } else {
//      if (!digitalRead(masterSwitch)) {
//        setup();
//      } else {
//        dbpl("swtich the master switch to reset");
//      }
//    }
//  }
  // pivot(90);
  //  hcf();
  //  delay(10000);
}
