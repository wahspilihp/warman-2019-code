#include <Arduino.h>
#include <Tic.h>
#include <math.h>
#include <limits.h>
// Following the Tic library directions:
// On boards with a hardware serial port available for use, use
// that port to communicate with the Tic. For other boards,
// create a SoftwareSerial object using pin 10 to receive (RX)
// and pin 11 to transmit (TX).

#ifdef SERIAL_PORT_HARDWARE_OPEN
#define ticSerial SERIAL_PORT_HARDWARE_OPEN
#else
#error Software serial not configured
#endif
#define DEBUG 1x
#ifdef DEBUG
#define test_delay(t)  delay(t);
#define dbp(str) \
  do{ \
    Serial.print(__PRETTY_FUNCTION__); \
    Serial.print("\t"); \
    Serial.print(__LINE__); \
    Serial.print(":\t");\
    Serial.print(str);\
  }while(false)
#define dbpl(str) \
  do{ \
    Serial.print(__PRETTY_FUNCTION__); \
    Serial.print("\t"); \
    Serial.print(__LINE__); \
    Serial.print(":\t");\
    Serial.println(str);\
  }while(false)
  #define dbpf(...) \
  do{ \
    Serial.print(__PRETTY_FUNCTION__); \
    Serial.print("\t"); \
    Serial.print(__LINE__); \
    Serial.print(":\t");\
    Serial.printf(__VA_ARGS__);\
  }while(false)
#define dbpv(var) \
  do{ \
    Serial.print(__PRETTY_FUNCTION__); \
    Serial.print("\t"); \
    Serial.print(__LINE__); \
    Serial.print(":\t");\
    Serial.print(#var);\
    Serial.print("\t=\t");\
    Serial.println(var);\
  }while(false)
#else
#define test_delay(t) ((void)0)
#define dbp(str) ((void)0)
#define dbpl(str) ((void)0)
#define dbpf(...) ((void)0)
#define dbpv(var) ((void)0)
#endif

#define masterSwitch  0
uint8_t phase = 0;

const TicStepMode mode = TicStepMode::Microstep8;
const TicStepMode screwMode = TicStepMode::Microstep8;
//const uint32_t maxSpeed         =  7885388;//﻿78853883; //was ﻿13429000
const uint32_t maxSpeed         =  78853883;
//const uint32_t absoluteMaxSpeed = ﻿500000000;
const uint32_t absoluteMaxSpeed = 500000000;
const uint32_t maxAccel = 240000;//300000;
const uint32_t maxDecel = 960000;
const bool reverse = false;
const double DIAMETER = 80.0;
// const double PI = 3.141592653589793;
const int FULL_STEPS = 200;
const double WHEEL_SPACING = 325;
#define DEG_TO_RAD_F(deg) ((typeof(deg)) ((deg) * PI/180.0))
#define DIR (reverse ?  -1 : 1)
const double SLIP = 0.0;
const double STEPS_PER_REV = (double) (FULL_STEPS * (1 << (int) mode));
const double SCREW_STEPS_PER_REV = (double) (FULL_STEPS * (1 << (int) mode));
const double MM_PER_STEP = DIAMETER * PI / STEPS_PER_REV * (1.0 - SLIP);
const double STEP_PER_MM = 1 / MM_PER_STEP;
const uint32_t STEP_PER_NM = ((uint32_t) (1000.0 * STEP_PER_MM));
// #define MM_TO_STEPS(mm) ((int32_t)((double)mm)/MM_PER_STEP)
inline int32_t MM_TO_STEPS(double mm)
{
  double dsteps = DIR * mm / MM_PER_STEP;
  return (int32_t) dsteps;
}
inline int32_t screwmmToSteps(double mm){
  return (int32_t)(ceil(mm*SCREW_STEPS_PER_REV/8.0));
}


#define MS1_PIN 20
void ms1_isr(){
  dbpl("ms1 triggered");
}
#define MS2_PIN 21
void ms2_isr(){
  dbpl("ms2 triggered");
}
/*
  Each Tic500 has its own address, as listed below, and has a secondary group address.
  Requests for data, that is get...() functions, should always be sent to an individual Tic, becasue the response will be nonsense if the messages are not identical.
  Commands, that is set...() functions, should usually be sent to a pair
*/
TicSerial left_front(ticSerial, 11);
TicSerial left_back(ticSerial, 12);
TicSerial left(ticSerial, 10);

TicSerial right_front(ticSerial, 21);
TicSerial right_back(ticSerial, 22);
TicSerial right(ticSerial, 20);

TicSerial arm_open(ticSerial, 31);
TicSerial arm_lift(ticSerial, 32);
TicSerial arm(ticSerial, 30);

/*
  Track poisition on the arduino to save polling the Tics.
*/
int32_t leftPos = 0;
int32_t rightPos = 0;
int32_t armOPos = 0;
int32_t armLPos = 0;
int32_t armOTarget = 0;
int32_t armLTarget = 0;

void printPos(){
//  dbpf("left_front.getTargetPosition()\t=\t#i",   left_front.getTargetPosition()));
//  dbpf("left_front.getCurrentPosition()\t=\t#i",  left_front.getCurrentPosition()));
//  dbplf("right_front.getTargetPosition()\t=\t#i",  right_front.getTargetPosition()));
//  dbplf("right_front.getCurrentPosition()\t=\t#i", right_front.getCurrentPosition()));
//  dbpf("arm_lift.getTargetPosition()\t=\t#i",     arm_lift.getTargetPosition()));
//  dbpf("arm_lift.getCurrentPosition()\t=\t#i",    arm_lift.getCurrentPosition()));
//  dbpf("arm_open.getTargetPosition()\t=\t#i",     arm_open.getTargetPosition()));
//  dbpf("arm_open.getCurrentPosition()\t=\t#i",    arm_open.getCurrentPosition()));
////  dbpl(right_front.getCurrentPosition());
//  dbpv(left_front.getTargetPosition());
//  dbpv(left_front.getCurrentPosition());
//  dbpv(right_front.getTargetPosition());
//  dbpv(right_front.getCurrentPosition());
//  dbpv(arm_lift.getTargetPosition());
//  dbpv(arm_lift.getCurrentPosition());
//  dbpv(arm_open.getTargetPosition());
//  dbpv(arm_open.getCurrentPosition());
}

inline bool openDone(){
  return (arm_open.getCurrentPosition() == arm_open.getTargetPosition());
}
inline bool liftDone(){
  if (arm_lift.getCurrentPosition() == armLTarget){
    dbpl("lifting done");
    arm_lift.deenergize();
    return true;
  }else{
    dbpl("not done lifitng");
    dbpl(arm_lift.getCurrentPosition());
    dbpl(arm_lift.getTargetPosition());
    return false;
  }
}
void openArm(bool open = true){
  dbpl(open);
  arm_open.energize();
  arm_open.clearDriverError();
  arm_open.exitSafeStart();
  arm_open.haltAndSetPosition(0);
//  arm_open.setMaxSpeed(absoluteMaxSpeed);
  screwDefaults();
  arm_open.setMaxSpeed(78853883);
  arm_open.clearDriverError();
  arm_open.exitSafeStart();
  if (open){
    armOTarget = screwmmToSteps(300);
  }else{
    dbpl("closing arm");
  }
  arm_open.setTargetPosition(armOTarget);
}
void liftArm(double mm){
  dbpv(mm);
  dbpv(screwmmToSteps(mm));
  arm_lift.energize();
  screwDefaults();
  arm_lift.setMaxSpeed(mm>0?13429000:78853883);
  arm_lift.haltAndSetPosition(0);
  arm_lift.exitSafeStart();
  
  armLPos = 0;
  armLTarget = 0;
  dbpv(arm_lift.getTargetPosition());
  dbpv(arm_lift.getCurrentPosition());
  armLPos += screwmmToSteps(mm);
  arm_lift.setTargetPosition(screwmmToSteps(mm));
//  dbpv(armLTarget);
  dbpv(arm_lift.getTargetPosition());
}

void setup()
{
//  openArm();
  phase = 0;
#ifdef DEBUG
  Serial.begin(115200);
#endif
  pinMode(masterSwitch, INPUT_PULLUP);
  // pinMode(2, INPUT);
  // pinMode(3, INPUT);
  // motors.flipM1(true);
  // attachInterrupt(digitalPinToInterrupt(LIFT_ENCODER), lift_upencoder, RISING);
  // attachInterrupt(digitalPinToInterrupt(CURVE_ENCODER), curve_upencoder, RISING);
  
 pinMode(MS1_PIN, INPUT_PULLUP);
 attachInterrupt(digitalPinToInterrupt(MS1_PIN), ms1_isr, LOW);
 pinMode(MS2_PIN, INPUT_PULLUP);
 attachInterrupt(digitalPinToInterrupt(MS2_PIN), ms2_isr, LOW);
  digitalWrite(13, LOW);
  // Set baud rate for serial comms
  ticSerial.begin(115385);
  // Pause to allow comms to be established: required by Pololu protocol
  delay(20);
  hsp();
  arm_open.haltAndSetPosition(0);
  arm_lift.haltAndSetPosition(0);
  // Clear any errors which may have occurred: in the competiton there's no way we can fix a problem anyway ,so just hope the problem will go away if we ignore it.
  left.clearDriverError();
  right.clearDriverError();
  arm.clearDriverError();
  // Unlock the motors to allow commands to be acted upon
  left.exitSafeStart();
  right.exitSafeStart();
  arm.exitSafeStart();
  // Set current limits etc. and energize motors
  keyDefaults(left);
  keyDefaults(right);
  keyDefaults(arm);
  //The arm opener uses a different motor with a higher current limit.
  arm_open.setCurrentLimit(1700);
      // Pause to allow the USB cable to be removed etc.
  for (int i =10; i>0; --i){
    dbpl(i);
    delay(1000);
  }
  left.energize();
  right.energize();
  arm_lift.energize();
  // Clear any errors which may have occurred: in the competiton there's no way we can fix a problem anyway ,so just hope the problem will go away if we ignore it.
  left.clearDriverError();
  right.clearDriverError();
  arm.clearDriverError();
  left.exitSafeStart();
  right.exitSafeStart();
  arm.exitSafeStart();
  // Set speed etc. limits
  restoreDefaults(left);
  restoreDefaults(right);
  restoreDefaults(arm);
  screwDefaults();
  hsp();
  arm_open.haltAndSetPosition(0);
  arm_lift.haltAndSetPosition(0);

  dbpl("Thunderbirds are go");
//  openArm();
//  delay(10000);


  // Clear errors again
  left.clearDriverError();
  right.clearDriverError();
  arm.clearDriverError();
  left.exitSafeStart();
  right.exitSafeStart();
  arm.exitSafeStart();
  digitalWrite(13, LOW);
  dbpl("started");
}

/*
  Convenience method to stop the motors and reset the position to 0
*/
void hsp()
{
  left.haltAndSetPosition(0);
  right.haltAndSetPosition(0);
  leftPos = 0;
  rightPos = 0;
}

/*
  Sets the essential settings which should not be changed for a motor
*/
void keyDefaults(TicSerial & which)
{
  which.setProduct(TicProduct::T500);
  which.setCurrentLimit(1200);
  which.haltAndSetPosition(0);
}

/*
  set those defaults which can reasonably be overridden during operation
*/
void restoreDefaults(TicSerial & which)
{
  which.setStepMode(mode);
  which.setMaxSpeed(maxSpeed);
  which.setMaxAccel(maxAccel);
  which.setMaxDecel(maxDecel);
  dbpv(which.getMaxAccel());
  dbpv(which.getMaxDecel());
}

void screwDefaults(){
  arm.setStepMode(screwMode);
  arm.setMaxSpeed(500000000);
//  arm_open.setMaxSpeed(2000000);
  arm.setMaxAccel(214748);
//  arm_open.setMaxAccel(960000);
//  arm_open.setMaxSpeed(
//  arm.setMaxDecel(2147483647);
  arm.setMaxDecel(2147483647);
  arm_open.setCurrentLimit(1700);
//  arm_open.deenergize();
}

/*
  Drive the specified distance in millimeters.
  Note: this overshoots slightly, by around 1%, as the wheels are marginally oversized.
*/
bool drive(double millimetres)
{
  dbpl(millimetres);
  // Calculate the target positions
  int32_t left_target = leftPos + MM_TO_STEPS(millimetres);
  int32_t right_target = rightPos + MM_TO_STEPS(millimetres);
  left.setTargetPosition(left_target);
  right.setTargetPosition(right_target);
  printPos();
  waitForPosition(right_front, right_target);
//    delay(abs(20*millimetres));
    dbpl("right pos");
  waitForPosition(left_front, left_target);
    dbpl("leftpos");
    
  leftPos = left_target;
  rightPos = right_target;
  return true;
}

// inline uint32_t reduceByRatio(uint32_t base, double ratio){
// return (uint32_t)(((double) base)/ratio);
// }
#define reduceByRatio(base, ratio) ((typeof(base)) (((typeof(ratio)) base) /ratio))
// Positive radius, positive angle is forwards
/*

*/
bool curve(double radius, double angle, bool turn_right)
{
  double theta = DEG_TO_RAD_F(angle);
  // assert(radius >= 0);
  // The ratio of inner and outer distances is given by -(B + 2*r)/(B - 2*r) if their slip is constant
  if (WHEEL_SPACING == 2 * radius)
  {
    radius += 1;
  }
  double ratio = - (WHEEL_SPACING + 2 * radius) / (WHEEL_SPACING - 2 * radius);
  uint32_t inner_speed = reduceByRatio(maxSpeed, ratio);
  uint32_t inner_accel = reduceByRatio(maxAccel, ratio);
  uint32_t inner_decel = reduceByRatio(maxDecel, ratio);
  int32_t outer_distance = radius * DEG_TO_RAD_F(angle) + WHEEL_SPACING / 2;
  int32_t inner_distance = radius * DEG_TO_RAD_F(angle) - WHEEL_SPACING / 2;
  TicSerial inner = turn_right ? right : left;
  inner.setMaxAccel(inner_accel);
  inner.setMaxSpeed(inner_speed);
  inner.setMaxDecel(inner_decel);
  restoreDefaults(inner);
  return false;
}

/*
  Pivots about the centre of the chassis.
  Positive angles are clockwise when viewed from above.
  Becuase no allowance has yet been made for wheelslip, the rotation is approximately 3/4 of the specificed angle in degrees.
*/
bool pivot(double angle)
{
  dbpl(angle);
  // The angular velocity = 2v/(WHEEL_SPACING)
  // therefore,  angle = 2v/(WHEEL_SPACING)*t = 2d/(WHEEL_SPACING)
  angle = DEG_TO_RAD_F(angle);
  double millimetres = WHEEL_SPACING * angle / 2.0*1.5;
  // Note that unlike in drive() the distance is added to the right but subtracted from the left
  int32_t left_target = leftPos - MM_TO_STEPS(millimetres);
  int32_t right_target = rightPos + MM_TO_STEPS(millimetres);
  // //  Reduce speed and acceleration for pivoting becuase of the higher torque required.
  // uint32_t pivot_accel = maxAccel/16;
  // uint32_t pivot_speed = maxSpeed/16;
  // left.setMaxAccel(pivot_accel);
  // right.setMaxAccel(pivot_accel);
  // left.setMaxSpeed(pivot_speed);
  // right.setMaxSpeed(pivot_speed);
  // This is the same as in drive()
  left.setTargetPosition(left_target);
  right.setTargetPosition(right_target);
  waitForPosition(right_front, right_target);
  waitForPosition(left_front, left_target);
  // Update the cached position
  leftPos = left_target;
  rightPos = right_target;
  // reset the speed and acceleration limits to the normal values
  restoreDefaults(left);
  restoreDefaults(right);
  return true;
}

void waitForPosition(TicBase & which, int32_t targetPosition){
  waitForPosition(which, targetPosition, 0);
}

// From the Tic Library examples:
// Polls the Tic, waiting for it to reach the specified target
// position.  Note that if the Tic detects an error, the Tic will
// probably go into safe-start mode and never reach its target
// position, so this function will loop infinitely.  If that
// happens, you will need to reset your Arduino.
//
// Modified to add tolerance: 
//  - if tolerance is 0, waits for position == target
//  - if tolerance is positive, waits for any position >= target
//  - if tolerance is negative, waits for any position <= target
void waitForPosition(TicBase & which, int32_t targetPosition, int8_t tolerance)
{
  do
  {
    which.resetCommandTimeout();
    printPos();
  }
  while (
    (tolerance==0)?
      which.getCurrentPosition() != targetPosition
      :((tolerance < 0)?
        which.getCurrentPosition() > targetPosition
        :which.getCurrentPosition() < targetPosition
      )
    );
}

/*
  Halt and disable operation: this effectively prevents the loop continuing.
*/
void hcf()
{
  right.deenergize();
  left.deenergize();
  arm.deenergize();
}



void loop()
{
  dbpl(phase);
  if (phase == 0) {
    //Loop until activation switch is fired
    if (digitalRead(masterSwitch)) {
      phase = 1;
      dbpl("activation switch on");
    } else {
      dbpl("activation switch is off");
      test_delay(5000);
    }
  }
  if (phase == 1) {
    // dbpl("advancing out of garage");
    // drive(-200);
    // dbpl("nose out, activating curve");
    // openArm();
    // drive(-300);
    double millimetres=-500;
    int32_t left_target = leftPos + MM_TO_STEPS(millimetres);
    int32_t right_target = rightPos + MM_TO_STEPS(millimetres);
    left.setTargetPosition(left_target);
    right.setTargetPosition(right_target);
    printPos();
    waitForPosition(right_front, MM_TO_STEPS(-200), -1);
//    delay(abs(20*millimetres));
    dbpl("right out");
    waitForPosition(left_front, MM_TO_STEPS(-200), -1);
    dbpl("left out");
    printPos();
    openArm();
    waitForPosition(right_front, right_target);
//    delay(abs(20*millimetres));
    dbpl("right pos");
    waitForPosition(left_front, left_target);
    dbpl("leftpos");
    leftPos = left_target;
    rightPos = right_target;
    dbpl("fully out");

    pivot(-30);
    dbpl("B");
    drive(-140);
    dbpl("C");
    pivot(30);
//    liftArm(80);
    dbpl("D");
    drive(-225);
    dbpl("E");
    dbpl("driven to ocean compound, lowering arm");
    left.deenergize();
    right.deenergize();
    liftArm(-30);
    delay(4000);
    phase = 2;
  }
  if (phase == 2) {
    if (!openDone() || arm_lift.getCurrentPosition()!=arm_lift.getTargetPosition()) {
      if (!openDone()) {
        dbpl("curve not done, waiting");
        dbpv(arm_open.getCurrentPosition());
        dbpv(arm_open.getTargetPosition());
      }
      if (arm_lift.getCurrentPosition()!=arm_lift.getTargetPosition()) {
        dbpl("lift not done, waiting");
        dbpv(arm_lift.getCurrentPosition());
        dbpv(arm_lift.getTargetPosition());
      }
      test_delay(2000);
    } else {
      dbpl("lifing arm back up");
//      liftArm(110);
//      liftArm(110+250+100+45+95-40);
    arm_lift.setTargetPosition(11000);
      // test_delay(3000);
      delay(1000);
      dbpv(arm_lift.getCurrentPosition());
      dbpv(arm_lift.getTargetPosition());
      phase = 3;
    }
  }
  if (phase == 3) {
    if (arm_lift.getCurrentPosition()==arm_lift.getTargetPosition()) {
      dbpl("arm has cleared ocean compound wall, moving to deposit");
      right.energize();
      left.energize();
      hsp();
      phase = 4;
    } else {
      dbpl("waiting for the arm to clear the ocean compound wall");
      dbpv(arm_lift.getCurrentPosition());
      dbpv(arm_lift.getTargetPosition());
    }
  }
  if (phase == 4) {
    dbpl("start lifting the arm to clear the inland compound wall");
//    liftArm(80);
//liftArm(160);//`````````````````````````````````````````````````````````````````````````````````````````````````````````````````
arm_lift.setTargetPosition(23000);
//    delay(2000);
    dbpl("reverse to give space to turn");
    drive(150);
    dbpl("G");
    pivot(63);
    dbpl("H");
    drive(550);
    dbpl("I");
    pivot(-70);
    dbpl("J");
    drive(1200);
    dbpl("we can't go any further until the arm is fully up");
    phase = 5;
  }
  if (phase == 5) {
    if (arm_lift.getCurrentPosition()!=arm_lift.getTargetPosition()) {
      dbpl("waiting to clear the inland compound wall");
      dbpl(arm_lift.getCurrentPosition());
      dbpl(arm_lift.getTargetPosition());
//      test_delay(2000);
    } else {
      dbpl("advance to the inland compound");
      //  dbpl("K");
      pivot(-135);
      dbpl("L");
      drive(-300);
      dbpl("M");
//      pivot(5);
      dbpl("N");
//      drive(-130);
      dbpl("O");
      left.setMaxDecel(INT_MAX);
      right.setMaxDecel(INT_MAX);
      drive(-150);
      dbpl("lower arm into the inland compund");
      liftArm(-70);
      delay(4000);
      phase = 6;
    }
  }
  if (phase == 6) {
    if (arm_lift.getCurrentPosition()!=arm_lift.getTargetPosition()) {
      dbpl("waiting for the deposit");
      dbpl(arm_lift.getCurrentPosition());
      dbpl(armLTarget);
      test_delay(1000);
    } else {
      dbpl("done!!!!!!");
      hcf();
      phase = 7;
    }
  }
  if (phase == 7) {
    if (digitalRead(masterSwitch)) {
      dbpl("I'm done, waiting to be reset");
      delay(5000);
    } else {
      liftArm(30-90-90);
      openArm(false);
      phase = 8;
    }
  }
  if (phase == 8) {
    if (!openDone() || !liftDone()) {
      if (!openDone()) {
        dbpl("curve not done, waiting");
      }
      if (!liftDone()) {
        dbpl("lift not done, waiting");
      }
      test_delay(2000);
    } else {
      if (!digitalRead(masterSwitch)) {
        dbpl("master switch set to reset");
//        setup();
      } else {
        dbpl("switch the master switch to reset");
      }
    }
  }
  if (phase==255){
    dbpl("shutdown");
    hcf();
  }
  // pivot(90);
  //  hcf();
  //  delay(10000);
}
